# dada2nephele R package README

-   this is an [renv](https://rstudio.github.io/renv/index.html) environment for DADA2 v1.26.0 with a few extra packages/scripts for running the Nephele DADA2 pipeline on Biowulf in */data/BCBB_microbiome_db/software/dada2/1.26.0*.

- **tl;dr:** if you just want to use DADA2, without any pipeline crap:

  - load R module ``module load R/4.2.2``

  - and in your R script:

    ```R
    source("/data/BCBB_microbiome_db/software/dada2/1.26.0/.Rprofile", chdir=T)
    library(dada2)
    ...whatever...
    ```

    You can run the script from wherever.  The ``source`` is just to load the environment, kinda like activating a conda environment

    - (and like activating conda, it takes a strange amount of time - sometimes long, sometimes short. not sure where it is caching)

-   databases are in */data/BCBB_microbiome_db/DB-rRNA*

### More info

-   It DOES change your ``.libPaths`` , so packages by default will install to this environment.  I have fixed the repos to [Bioconductor 3.16](https://bioconductor.org/packages/3.16/BiocViews.html#___Software) and [MRAN snapshot from Jan 4 2023](https://mran.microsoft.com/snapshot/2023-01-04).  Feel free to install to this environment using the defaults with ``install.packages``.  Installing from github using ``remotes``  should also work.

    -   As a test of reproducibility, it should not overwrite any of the existing libs with other versions!  The cache keeps package versions separate, so it should be easy to roll back if necessary.  We will see... I am not very precious about this, mostly interested in seeing how this renv works, so let me know what happens, but don't be worried.

-   use the ``lib.loc`` parameter of [``install.packages``](https://search.r-project.org/R/refmans/utils/html/install.packages.html) if you want to install somewhere else other than this environment.  if you want to install with different repo using ``repos`` arg, *please install somewhere else*  not big deal - if forget, let me know!



### If you want to run the pipeline, read on.

-   **Function reference:** [dada2nephele R package
    manual](doc/Reference_Manual_dada2nephele.md)
    
-   **R:** To use the dada2nephele package locally on your machine or
    server like Biowulf, see the example script,
    [standalone.R](scripts/standalone.R)
    and the [dada2compute function help](doc/Reference_Manual_dada2nephele.md#dada2compute).

-   [Nephele user docs](https://nephele.niaid.nih.gov/details_dada2/)

-   **Python:** Function to be used in python with rpy2 is
    `trycomputewrapper`. See [trycomputewrapper help](doc/Reference_Manual_dada2nephele.md#trycomputewrapper).

-   Will need to pass the location of the reference database in the
    function call to `dada2compute`. See [refdb and refdb_species parameters](doc/Reference_Manual_dada2nephele.md#dada2compute).

-   The global constants are set to an R option `dparams` when the
    package loads. This includes default values for parameters not set
    by the user as well as those that are user options. See `onLoad`
    [help](doc/Reference_Manual_dada2nephele.md#onload).

